package linkedlist;

import java.util.Scanner;

public class Console {
	public static String readString(String msg) {
		Scanner input = new Scanner(System.in);
		System.out.print(msg);
		return input.nextLine();		
	}	
}
