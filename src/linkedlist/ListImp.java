package linkedlist;

import java.util.LinkedList;

public class ListImp<E> extends LinkedList<E>{	
	private static final long serialVersionUID = 1L;
	private Node head;
	private Node tail;
	private int total;
	private Object[] indexTable;

	public ListImp() {}		

	public Object[] getArray() {
		return indexTable;
	}

	public void setArray(Object[] array) {
		this.indexTable = array;
	}

	public void add(String i) {
		if (isEmpty() == true) {
			addFirst(i);
			head = tail;

		} else {
			Node newElement = new Node(i);
			tail.setNext(newElement);
			tail = newElement;
			total++;
		}
	}

	public void addFirst(String i) {
		Node newElement = new Node(i);
		head = tail;

		if(isEmpty() == true){
			tail = newElement;
		}
		total++;
	}

	public void printList(ListImp<String> tmp){
		Node t;
		System.out.println(tmp);
		if (head!=null){
			System.out.println("All items in the list:");
			for (t = head; t != null; t = t.getNext()){
				System.out.println(t.getElement());
			}
		}
		else System.out.println("No item found!");
	}

	public boolean removeNode(String n){
		Node actual =  this.head;
		Node previous = null;
		if(isEmpty()){
			return false;
		} else {
			while ((actual != null) && (!actual.getElement().equals(n))){
				previous = actual;
				actual = actual.getNext();
			}
			if (actual == this.head){
				if(this.head == this.tail){
					this.tail = null;
				}

				this.head = head.getNext();
			} else {
				if (actual== this.tail){
					this.tail = previous;
				}
				previous.setNext(actual.getNext());
			}
			this.total--;
			return true;		
		}	
	}	

	public void searchList(String tmp) {
		int index = -1;	
		for (int i=0;i<getArray().length;i++) {
			if (indexTable[i].equals(tmp)) {
				index = i;
				break;
			}
		}
		if (index >= 0)
			System.out.println("Search result:\nIndex "+index+". "+indexTable[index]);
		else 
			System.out.println("Dado não encontrado!");

	}

	public void indexList(ListImp<String> tmp) {
		Node t;
		setArray(tmp.toArray(new Object[total]));
		int i = 0;
		for (t = head; t != null; t = t.getNext()){
			indexTable[i] = t.getElement();				
			i++;
		}
	}

	public boolean isEmpty(){
		if (this.head == null)
			return true;
		else
			return false;
	}	
	
	public int size(){
		return total;
	}

	@Override
	public String toString() {
		return "List resume:\nSize: " + total + ", First: " + head + ", Last: "
				+ tail;
	}
}