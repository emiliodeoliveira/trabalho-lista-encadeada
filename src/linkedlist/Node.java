package linkedlist;

public class Node {
	private String element;
	private Node previous;
	private Node next;
	private Node index;

	public Node() {

	}
	public Node(String i) {
		this.element=i;
	}

	public void setNext(Node newElement) {
		this.next = newElement;
	}

	public Node getNext () {
		return next;
	}

	public String getElement () {
		return element;
	}	
	
	public Node getPrevious() {
		return previous;
	}
	
	public void setPrevious(Node previous) {
		this.previous = previous;
	}
	
	@Override
	public String toString() {
		return element;
	}
}
