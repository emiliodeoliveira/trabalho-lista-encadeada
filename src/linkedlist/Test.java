package linkedlist;

public class Test {
	public static ListImp<String> list = new ListImp<>();		
	public static void main(String[] args) {
		initializeValues();
		list.indexList(list);
		while (true) {
			switch (Console.readString(menu()).charAt(0)) {
			case '1':
				list.add(Console.readString("Enter the value of element: "));
				list.indexList(list);
				break;
			case '2':
				list.removeNode(Console.readString("Enter the value of element: "));
				list.indexList(list);
				break;
			case '3':				
				list.searchList(Console.readString("Enter the value of element: "));
				break;
			case '4':							
				list.printList(list);
				break;
			case '5':
				System.exit(0);
				break;
			}
		}		
	}

	private static void initializeValues() {
		list.add("Jo�o");
		list.add("Eduardo");
		list.add("Jos�");
		list.add("Maria");
		list.add("Henrique");
		list.add("Rafael");	
	}

	private static String menu() {
		String menu = "";
		menu += "\nDoubly linked list manager:";
		menu += "\n  1 - Insert element.";
		menu += "\n  2 - Delete element.";
		menu += "\n  3 - Search.";
		menu += "\n  4 - Show the list of elements.";
		menu += "\n  5 - Sair.";
		menu += "\nChoose: ";
		return menu;
	}
}